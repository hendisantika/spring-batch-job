package com.hendisantika.springbatchjob;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-batch-job
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-01
 * Time: 10:46
 * To change this template use File | Settings | File Templates.
 */
public class Transaction {
    private String transactionId;
    private String merchantId;
    private String transactionAmt;
    private String merchantName;

    public Transaction(String transactionId, String merchantId, String transactionAmt) {
        this.transactionId = transactionId;
        this.merchantId = merchantId;
        this.transactionAmt = transactionAmt;
    }

    public Transaction() {
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTransactionAmt() {
        return transactionAmt;
    }

    public void setTransactionAmt(String transactionAmt) {
        this.transactionAmt = transactionAmt;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId='" + transactionId + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", transactionAmt='" + transactionAmt + '\'' +
                ", merchantName='" + merchantName + '\'' +
                '}';
    }
}
