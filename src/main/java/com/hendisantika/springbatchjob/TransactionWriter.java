package com.hendisantika.springbatchjob;

import org.springframework.batch.item.ItemWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-batch-job
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-01
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 */
public class TransactionWriter implements ItemWriter<Transaction> {
    @Override
    public void write(List<? extends Transaction> items) throws Exception {
        List<String> enrichedTxnList = new ArrayList<>();
        items.forEach(item -> {
            String enrichedTxn = String.join(",", item.getTransactionId(), item.getMerchantId(), item.getMerchantName(), item.getTransactionAmt());
            enrichedTxnList.add(enrichedTxn);
        });
        enrichedTxnList.forEach(System.out::println);
        //Files.write(Paths.get("./output/transaction-enriched.txt"), enrichedTxnList, StandardOpenOption.CREATE,StandardOpenOption.APPEND);
    }
}