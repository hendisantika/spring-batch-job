package com.hendisantika.springbatchjob;

import org.springframework.batch.item.ItemProcessor;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-batch-job
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-01
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 */
public class TransactionProcessor implements ItemProcessor<Transaction, Transaction> {
    @Override
    public Transaction process(Transaction item) throws Exception {

        if ("1001".equalsIgnoreCase(item.getMerchantId())) {
            item.setMerchantName("Amazon");
        } else if ("1002".equalsIgnoreCase(item.getMerchantId())) {
            item.setMerchantName("Walmart");
        } else {
            item.setMerchantName("Not Available");
        }
        System.out.println("Enriched Trasaction Details --> " + item.toString());
        return item;
    }
}